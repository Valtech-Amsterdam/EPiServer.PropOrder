#!/bin/bash

# Publish the NuGet publish command with the apropriate tag specified in the git TagName
# ${companyName}.${vendorName}.${CI_PROJECT_NAME}.$(echo ${CI_BUILD_REF_NAME}
# I realize this file is redundand but I wanted it to be equal to the JS version

# Validate variable
if [ -z "${CI_BUILD_TAG}" ] ; then
    echo "Please make sure the '\${CI_BUILD_TAG}' variable is set!";
    exit 1;
fi

# Publish based on TagName
if [[ "${CI_BUILD_TAG}" =~ (\-dev){1} ]] ; then 
	nuget pack "./src/${companyName}.${vendorName}.${CI_PROJECT_NAME}/${companyName}.${vendorName}.${CI_PROJECT_NAME}.nuspec" -version ${CI_BUILD_TAG} ${nugetPackTags};
	nuget push ./${companyName}.${vendorName}.${CI_PROJECT_NAME}.${CI_BUILD_TAG}.nupkg ${nugetPushTags};

    echo "Published development package '${companyName}.${vendorName}.${CI_PROJECT_NAME}.${CI_BUILD_TAG}'";
    exit 0;
fi
if [[ "${CI_BUILD_TAG}" =~ (\-alpha){1} ]] ; then 
	nuget pack "./src/${companyName}.${vendorName}.${CI_PROJECT_NAME}/${companyName}.${vendorName}.${CI_PROJECT_NAME}.nuspec" -version ${CI_BUILD_TAG} ${nugetPackTags};
	nuget push ./${companyName}.${vendorName}.${CI_PROJECT_NAME}.${CI_BUILD_TAG}.nupkg ${nugetPushTags};

    echo "Published alpha package '${companyName}.${vendorName}.${CI_PROJECT_NAME}.${CI_BUILD_TAG}'";
    exit 0;
fi
if [[ "${CI_BUILD_TAG}" =~ (\-beta){1} ]] ; then 
	nuget pack "./src/${companyName}.${vendorName}.${CI_PROJECT_NAME}/${companyName}.${vendorName}.${CI_PROJECT_NAME}.nuspec" -version ${CI_BUILD_TAG} ${nugetPackTags};
	nuget push ./${companyName}.${vendorName}.${CI_PROJECT_NAME}.${CI_BUILD_TAG}.nupkg ${nugetPushTags};

    echo "Published beta package '${companyName}.${vendorName}.${CI_PROJECT_NAME}.${CI_BUILD_TAG}'";
    exit 0;
fi
if [[ "${CI_BUILD_TAG}" =~ (\-rc){1} ]] ; then 
	nuget pack "./src/${companyName}.${vendorName}.${CI_PROJECT_NAME}/${companyName}.${vendorName}.${CI_PROJECT_NAME}.nuspec" -version ${CI_BUILD_TAG} ${nugetPackTags};
	nuget push ./${companyName}.${vendorName}.${CI_PROJECT_NAME}.${CI_BUILD_TAG}.nupkg ${nugetPushTags};

    echo "Published release candidate package '${companyName}.${vendorName}.${CI_PROJECT_NAME}.${CI_BUILD_TAG}'";
    exit 0; 
fi
if [[ "${CI_BUILD_TAG}" =~ ([1-9]\.[0-9]{1,}\.[0-9]{1,})$ ]] ; then 
	nuget pack "./src/${companyName}.${vendorName}.${CI_PROJECT_NAME}/${companyName}.${vendorName}.${CI_PROJECT_NAME}.nuspec" -version ${CI_BUILD_TAG} ${nugetPackTags};
	nuget push ./${companyName}.${vendorName}.${CI_PROJECT_NAME}.${CI_BUILD_TAG}.nupkg ${nugetPushTags};

    echo "Published final package '${companyName}.${vendorName}.${CI_PROJECT_NAME}.${CI_BUILD_TAG}'";
    exit 0; 
fi

# Extra error if someone is stubborn
echo "Unrecognized package version pattern '${CI_BUILD_TAG}'"; 
exit 2;