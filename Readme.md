[//]: # (Header)

<div align="center">
  <a href="https://gitlab.com/Valtech-Amsterdam/EPiServer.PropOrder">
    <img height="250" width="250" src="https://gitlab.com/Valtech-Amsterdam/EPiServer.PropOrder/raw/master/resource/PropOrder.png" alt="TypeScript Kit">
  </a>
</div>

[![Downloads][downloads-image]][nuget-url] 
[![Build Status][build-image]][build-url] [![Coveralls Status][coveralls-image]][coveralls-url]

[//]: # (Documentation)

## What is EPiServer.PropOrder?  
This is a Libarary providing an Attribute that uses a compiler feature to allow your EPiServer properties to be orderd the way your class properties are.

## Installing the library
Simply check out the [NuGet Package][nuget-url] using:  
```Batchfile
Install-Package Valtech.Amsterdam.Labs.EPiServer.PropOrder
```

### Prerequisites  
* You need to configure your project to compile to dotnet 4.6.1 or higher.  
  
## Using the library 
If you mark Properties on a Model class defining EPiServer PageData and you don't specify an order the module will take responsibility for the order.  
IF you take the following classes as an example (Click the thumbnails to enlarge):  

<div align="center">
  <a href="/documentation/2017-08-15_2140_001.png" alt="Click to view full size"><img width="200" src="/documentation/2017-08-15_2140_001.png" /></a>&nbsp;&nbsp;&nbsp;
  <a href="/documentation/2017-08-15_2140.png" alt="Click to view full size"><img width="200" src="/documentation/2017-08-15_2140.png" /></a>&nbsp;&nbsp;&nbsp;
  <a href="/documentation/2017-08-15_2140_002.png" alt="Click to view full size"><img width="200" src="/documentation/2017-08-15_2140_002.png" /></a>  
</div>
  
This will result in:  
  
![Rendered result](/documentation/2017-08-15_2139.png)  
  
Properties from base classes will be ordered below the inheriting classes Properties.  

As the images illustrate the order of the pages properties are ordered in the same order   
as the C# class without configuring the integer order value EPiServer requires you to.  
  
## [Contributing][contributing-url]  
[contributing-url]: https://gitlab.com/Valtech-Amsterdam/EPiServer.PropOrder/blob/master/Contributing.md
See the [Contribution guide][contributing-url] for help about contributing to this project.
  
## [Changelog][changelog-url]  
[changelog-url]: https://gitlab.com/Valtech-Amsterdam/EPiServer.PropOrder/blob/master/Changelog.md
See the [Changelog][changelog-url] to see the change history.

[//]: # (Labels)

[downloads-image]: https://img.shields.io/nuget/dt/Valtech.Amsterdam.Labs.EPiServer.PropOrder.svg
[nuget-url]: https://www.nuget.org/packages/Valtech.Amsterdam.Labs.EPiServer.PropOrder
[build-url]: https://gitlab.com/Valtech-Amsterdam/EPiServer.PropOrder/pipelines
[build-image]: https://gitlab.com/Valtech-Amsterdam/EPiServer.PropOrder/badges/master/build.svg
[coveralls-url]: https://gitlab.com/Valtech-Amsterdam/EPiServer.PropOrder/commits/master
[coveralls-image]: https://gitlab.com/Valtech-Amsterdam/EPiServer.PropOrder/badges/master/coverage.svg?job=test