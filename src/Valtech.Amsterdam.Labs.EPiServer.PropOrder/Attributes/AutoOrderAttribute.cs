﻿using System;
using System.Runtime.CompilerServices;
using Valtech.Amsterdam.Labs.EPiServer.PropOrder.Module;

namespace Valtech.Amsterdam.Labs.EPiServer.PropOrder.Attributes
{

    /// <summary>
    /// Marks this property to be ordered based on the order it appears in code
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public class AutoOrderAttribute : Attribute
    {
        private readonly int _order;

        /// <summary>
        /// Marks this property to be ordered based on the order it appears in code
        /// The <see cref="AutoPropertyOrderModule"/> should order this in order of code
        /// </summary>
        /// <param name="order">Injected by the compiler, please don't touch</param>
        public AutoOrderAttribute([CallerLineNumber]int order = 0)
        {
            _order = order;
        }

        /// <summary>
        /// Get the LineNumber injected by the compiler
        /// </summary>
        /// <returns></returns>
        public int GetOrder() => _order;
    }
}
