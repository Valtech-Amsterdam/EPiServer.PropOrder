﻿using System.ComponentModel.DataAnnotations;
using System.Reflection;
using EPiServer.Shell.ObjectEditing;

namespace Valtech.Amsterdam.Labs.EPiServer.PropOrder.Models
{
    /// <summary>
    /// Custom episerver mapping for linking pagetypes to classes
    /// </summary>
    public class EpiserverPropertyMapping
    {
        /// <summary>
        /// The Property value resolved by the EPiServer internal API
        /// </summary>
        public ExtendedMetadata ContentProperty { get; set; }
        /// <summary>
        /// The <see cref="PropertyInfo"/> reflected from the custom class structure of the model
        /// </summary>
        public PropertyInfo ModelProperty { get; set; }
        /// <summary>
        /// The <see cref="System.ComponentModel.DataAnnotations.DisplayAttribute"/> use by episerver to provide MetaData to the model
        /// </summary>
        public DisplayAttribute DisplayAttribute { get; set; }
        /// <summary>
        /// The configured <see cref="System.ComponentModel.DataAnnotations.DisplayAttribute.Order"/> (if any)
        /// </summary>
        public int? SortOrder { get; set; }
    }
}
