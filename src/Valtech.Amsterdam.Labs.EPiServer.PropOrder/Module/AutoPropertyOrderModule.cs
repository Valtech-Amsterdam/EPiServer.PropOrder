﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using EPiServer.Core;
using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using EPiServer.Shell.ObjectEditing;
using RestSharp.Extensions;
using InitializationModule = EPiServer.Web.InitializationModule;
using Valtech.Amsterdam.Labs.EPiServer.PropOrder.Attributes;
using Valtech.Amsterdam.Labs.EPiServer.PropOrder.Models;

namespace Valtech.Amsterdam.Labs.EPiServer.PropOrder.Module
{
    /// <summary>
    /// Module for moving around default episerver properties
    /// </summary>
    [InitializableModule,
        ModuleDependency(typeof(InitializationModule))]
    public class AutoPropertyOrderModule : IInitializableModule, IMetadataExtender
    {
        private static readonly IList<string> SystemPropertyMappings = typeof(ContentData).GetProperties()
            .Concat(typeof(BlockData).GetProperties())
            .Concat(typeof(PageData).GetProperties())
            .Concat(typeof(IContentData).GetProperties())
            .Concat(typeof(IContent).GetProperties())
            .Select(propertyInfo => propertyInfo.Name)
            .ToList();

        /// <inheritdoc cref="IInitializableModule"/>
        public void Initialize(InitializationEngine context)
        {
            if (context.HostType != HostType.WebApplication) return;
            var registry = context.Locate.Advanced.GetInstance<MetadataHandlerRegistry>();
            registry.RegisterMetadataHandler(typeof(ContentData), this);
        }

        /// <inheritdoc cref="IMetadataExtender"/>
        public void Uninitialize(InitializationEngine context) { }

        /// <summary>
        /// Modify metadata for episerver built-in properties,
        /// this can be used to move properties to other tabs or to hide them.
        /// </summary>
        /// <param name="metadata"></param>
        /// <param name="attributes"></param>
        public void ModifyMetadata(ExtendedMetadata metadata, IEnumerable<Attribute> attributes)
        {
            var containerType = metadata.Properties.FirstOrDefault()?.ContainerType;
            if (containerType == null) return;

            // This cannot be cached, it needs to load multiple times if it has nested blocks
            OrderPropertiesRecursive(metadata);
        }

        private static void OrderPropertiesRecursive(ExtendedMetadata metadata)
        {
            // Get the default property mappings
            var propertyMappings = SystemPropertyMappings
                .Concat(metadata.MappedProperties.Select(prop => prop.From))
                .Concat(metadata.MappedProperties.Select(prop => prop.To))
                .Distinct()
                .ToList();

            // Get all properties of this contentDataInstance
            var contentProperties = metadata.Properties
                // Filter out system
                .Where(contentProperty => !propertyMappings.Any(propertyName => propertyName.Equals(contentProperty.PropertyName)))
                .ToList();

            foreach (var contentProperty in metadata.Properties)
            {
                if (!typeof(ContentData).IsAssignableFrom(contentProperty.ModelType)) continue;
                OrderPropertiesRecursive((ExtendedMetadata)contentProperty);
            }

            SetPropertiesOrder(contentProperties);
        }

        private static void SetPropertiesOrder(IEnumerable<ModelMetadata> contentProperties)
        {
            var mappedProperties = contentProperties
                // Select properties
                .Select(modelMetadata => GetEpiserverPropertyMappings((ExtendedMetadata)modelMetadata))
                .Where(property => property != null)
                // Make sure the deeper down the inheritance chain, the lower the properties
                .OrderBy(GetInheritanceLevel)
                // Then order by SortOrder from the linenumber the compiler attribute gave us
                .ThenBy(property => property.SortOrder)
                .ToList();

            // Loop through the custom mapping POCO's and assign an index value accordingly
            var index = 0;
            foreach (var contentProperty in mappedProperties)
            {
                var configuredOrder = contentProperty.DisplayAttribute.GetOrder();
                if (configuredOrder.HasValue)
                {
                    contentProperty.ContentProperty.Order = configuredOrder.Value;
                    contentProperty.DisplayAttribute.Order = configuredOrder.Value;
                    index = configuredOrder.Value + 1;
                    continue;
                }
                contentProperty.ContentProperty.Order = index;
                contentProperty.DisplayAttribute.Order = index;
                index++;
            }
        }

        private static int? GetInheritanceLevel(EpiserverPropertyMapping property)
        {
            var inheritanceLevel = 0;
            var type = property.ModelProperty.ReflectedType;

            // ReSharper disable once PossibleNullReferenceException
            while (type.BaseType != null)
            {
                if (type.GetProperty(property.ModelProperty.Name) == null) return inheritanceLevel;
                inheritanceLevel++;
                type = type.BaseType;
            }
            return null; // null always comes last
        }

        private static EpiserverPropertyMapping GetEpiserverPropertyMappings(ExtendedMetadata modelMetadata)
        {
            if (modelMetadata == null) return null;
            var modelProperty = modelMetadata.ContainerType.GetProperty(modelMetadata.PropertyName);
            if (modelProperty == null) return null;
            // Todo: remove restsharp dependency
            var displayAttribute = modelProperty.GetAttribute<DisplayAttribute>();
            if (displayAttribute == null) return null;
            var defaultOrder = displayAttribute.GetOrder();
            // Todo: remove restsharp dependency
            var orderHelperAttribute = modelProperty.GetAttribute<AutoOrderAttribute>();
            if (orderHelperAttribute != null) defaultOrder = orderHelperAttribute.GetOrder();

            return new EpiserverPropertyMapping
            {
                ContentProperty = modelMetadata,
                ModelProperty = modelProperty,
                DisplayAttribute = displayAttribute,
                SortOrder = defaultOrder
            };
        }
    }
}
